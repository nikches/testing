#include "server.h"
#include "user.h"
#include "thread.h"

#include <QtNetwork>
#include <QtCore>

Server::Server(QObject *parent) : QTcpServer(parent){
    user_count = 0;
    users.clear();

    QDir task_path("tasks");
    if ( !loadTaskList(task_path.filePath("task.list")) )
        emit serverMessage("List of task not found!");
}

Server::~Server(){
    foreach(User *user,users)
        delete user;
}

qint32 Server::getUserCount(){
    return user_count;
}

void Server::connected(){
    QTcpSocket *client;
    while ((client= nextPendingConnection())){
        user_count++;
        users[client] = new User(client,this,task_list.count());
    }
}

void Server::disconnected(){
    QTcpSocket *client = (QTcpSocket *) sender();
    users[client]->setOnline(false);
    delete users[client];
    users.remove(client);
}

QString Server::checkName(const QString &name){   
    QString new_name = name;    

    for (int i = 0; i < new_name.count(); i++)
        if(new_name[i] == ' ') new_name[i] = '_';

    QTime   time = QTime::currentTime();
    foreach(User *user,users)
        if (user->getName() == new_name){
            new_name += "_" + QString::number(time.hour()) +
                              QString::number(time.minute()) +
                              QString::number(time.second()) +
                              QString::number(time.msec());

            break;
        }

    return new_name;
}

QList <User *> Server::getUsers(){
    QList <User *> user_list;
    foreach (User *user, users)
        user_list << user;
    return user_list;
}

QStringList Server::getTaskList(){
    return task_list;
}

bool Server::loadTaskList(const QString &name){
    QFile task_file(name);
    if ( !task_file.open(QIODevice::ReadOnly) )
        return false;

    task_list.clear();
    while ( !task_file.atEnd() )
        task_list << task_file.readLine().trimmed();
    task_file.close();
    return true;
}

void Server::sendAll(const QString &command, const QString &message){
    foreach(User *user, users)
        user->send(command,message);
}

QStringList Server::getUserList(){
    QStringList user_list;
    foreach (User *user, users)
        user_list << user->getName();
    return user_list;
}
