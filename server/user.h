#ifndef USER_H
#define USER_H

#include <QtCore>

class QTcpSocket;
class Server;

class User : public QObject
{
    Q_OBJECT
    bool             online;
    qint32           task_count;
    qint32           statistic_sum;
    QVector <qint32> statistic;
    QString          name;

    Server     *server;
    QTcpSocket *socket;
    QDir        user_dir;
    QDir        task_dir;

public:
    User(QTcpSocket *socket, Server *_server, qint32 _task_count);
    ~User();
    bool    isOnline();
    void    setOnline(bool status);

    void    sendExtension();
    void    send        (const QString &command, const QString &message);
    bool    sendTextFile(const QString &command, const QString &file_name);
    void    sendPoints  (const QString &task, const QVector<qint32> &points);
    bool    receiveFile (const QString &file_name, qint32 lines);

    void    createUserDir();
    void    removeUserDir();

    void    clearStatistic();
    void    setPoints(qint32 task_number, qint32 points);
    void    addPoints(qint32 task_number, qint32 points);
    void    subPoints(qint32 task_number, qint32 points);

    void    setName(const QString &_name);
    QString getName();
    QString getPointsSum();
    QString getPointsTask(int task);

signals:
    void    userMessage(const QString &name, const QString &message);
    void    changeState();
public slots:

    void    readyRead();
    void    programResult(const QString &task, const QVector <qint32> *points);
    void    compilationResult(const QString &message);
};

#endif // USER_H
