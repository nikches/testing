#include "form.h"
#include "server.h"
#include "user.h"

#include <QtWidgets>
#include <QDebug>

Form::Form(QWidget *parent) :
    QWidget(parent)
{

    tcp_server    = new Server(this);
    message_board = new QTextEdit();
    users         = new QTableWidget();

    users->setIconSize(QSize(16,16));
    message_board->setReadOnly(true);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(new QLabel("message board"));
    layout->addWidget(message_board);
    layout->addWidget(new QLabel("clients"));
    layout->addWidget(users);   

    setLayout(layout);
    setWindowTitle("server");    
    setGeometry(100,100,400,400);

    connect(tcp_server,SIGNAL(newConnection()), tcp_server,SLOT(connected()));
    connect(tcp_server,SIGNAL(serverMessage(const QString &)),this,SLOT(serverMessage(const QString &)));

    quint16 port = 4200;

    serverMessage(QString("welcome to testing server"));
    if (tcp_server->listen(QHostAddress::Any,port) == false){
        serverMessage("could not listen port " + QString::number(port));
        serverMessage("cay be other server listens on this port");
        tcp_server->close();
    } else
        serverMessage("listen port " + QString::number(port));

    prepairTable();
}


void Form::prepairTable(){
    user_icon = new QPixmap("user.png");
    QStringList    task_list = tcp_server->getTaskList();

    task_list.push_front("user name");
    task_list.push_front("sum");
    users->setColumnCount(task_list.count());
    users->setHorizontalHeaderLabels(task_list);
}

bool compareUserName(User *a, User *b){
    return a->getPointsSum() <= b->getPointsSum();
}

void Form::userChangeState(){
    QList <User *> user_list = tcp_server->getUsers();
    qSort(user_list.begin(),user_list.end(), compareUserName);
    QTableWidgetItem  *item;
    users->clearContents();
    users->setRowCount(0);

    foreach (User *user, user_list){
        if ( !user->isOnline() ) continue;
        users->insertRow(0);
        item = new QTableWidgetItem(user->getPointsSum());
        users->setItem(0,0, item);
        item = new QTableWidgetItem(user->getName());
        item->setIcon(*user_icon);
        users->setItem(0,1, item);

        for(int i = 0; i < users->columnCount()-2; i++){
            item = new QTableWidgetItem(user->getPointsTask(i));
            users->setItem(0,2+i,item);
        }
    }
}

void Form::serverMessage(const QString &message){
    QTime time = QTime::currentTime();
    message_board->append(
        "[" + QString::number(time.hour()) + ":" +
              QString::number(time.minute()) + ":" +
              QString::number(time.second()) + "]: " + message);
}

void Form::userMessage(const QString &name, const QString &message){
    QTime time = QTime::currentTime();
    message_board->append(
        "[" + QString::number(time.hour()) + ":" +
              QString::number(time.minute()) + ":" +
              QString::number(time.second()) + "] " + "[" + name + "]: "+message);
}

void Form::closeEvent(QCloseEvent *event){
     QMessageBox::StandardButton result;
     result = QMessageBox::question(this,"exit","you really want to quit?", QMessageBox::Yes | QMessageBox::No );
     if (result == QMessageBox::Yes){
         event->accept();
     }
     else
         event->ignore();
}
