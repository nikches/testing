#ifndef THREAD_H
#define THREAD_H

#include <QThread>

class User;
class Thread : public QThread
{
    Q_OBJECT
    QString m_source;
    QString m_user;

    void run();
public:

    bool         compareFiles  (const QString &file1,     const QString &file2);
    bool         buildProgram  (const QString &user_dir,  const QString &source);
    QString      selectCompiler(const QString &user_dir,  const QString &source);
    QVector <qint32> runProgram    (const QString &user_dir,  const QString &program);

    Thread(const User *user, const QString &user_name,const QString &source);
    ~Thread();


signals:
    void programResult(const QString &task, const QVector <qint32> *vector);
    void compilationResult(const QString &message);
};

#endif // THREAD_H
