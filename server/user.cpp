#include "user.h"
#include "server.h"
#include "thread.h"
#include <QList>
#include <QTcpSocket>
#include <QDebug>

User::User(QTcpSocket *_socket, Server *_server, qint32 _task_count) :QObject()
{
    name   = "no_name";
    online = false;
    statistic_sum = 0;
    socket = _socket;
    server = _server;
    task_count = _task_count;
    statistic.fill(0,task_count);

    user_dir.setPath("users");
    task_dir.setPath("tasks");
    user_dir.mkdir("noname");
    user_dir.cd("noname");

    connect(socket,SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket,SIGNAL(disconnected()), server,SLOT(disconnected()));

    connect(this,SIGNAL(changeState()),server->parent(),SLOT(userChangeState()));
    connect(this,SIGNAL(userMessage(QString,QString)),server->parent(),SLOT(userMessage(QString, QString)));
}

User::~User(){
    removeUserDir();    
    emit changeState();
}

void User::createUserDir(){    
    user_dir.mkdir(name);
    user_dir.cd(name);
}

void User::removeUserDir(){
    user_dir.removeRecursively();
    user_dir.cdUp();
}

void User::readyRead(){

    Thread *thread;
    QString command;
    QString text;
    QRegExp regExp("^/([a-z]*):(.*)$");

    while (socket->canReadLine()){
        if (regExp.indexIn(QString::fromUtf8(socket->readLine()).trimmed()) != -1){
            command = regExp.cap(1);
            text    = regExp.cap(2);

            if (command == "connect"){
                online = true;
                setName(server->checkName(text));
                continue;
            }

            if (command == "me"){
                setName(server->checkName(text));
                continue;
            }

            if (command == "file"){
                QStringList file_data = text.split(":");
                QString file_name  = file_data.first();
                qint32  line_count = file_data.last().toInt();
                receiveFile(file_name,line_count);
                thread = new Thread(this, name,file_name);
                thread->start();
                continue;
            }

            if (command == "message"){
                userMessage("message",text);
                continue;
            }

            if (command == "tasklist"){
                sendTextFile(command,task_dir.filePath("task.list"));
                continue;
            }

            if (command == "task"){
                sendTextFile(command,task_dir.filePath(text));
                continue;
            }

            if (command == "extension"){
                sendExtension();
                continue;
            }
        }
    }
}

bool User::receiveFile(const QString &file_name, qint32 line_count){
    QFile out(user_dir.filePath(file_name));
    if ( !out.open(QIODevice::WriteOnly) ){
        emit userMessage(name,"can't receive file " + file_name);
        return false;
    }
    while (line_count){
        if ( !socket->canReadLine() ){
            socket->waitForReadyRead();
            continue;
        }
        line_count--;
        out.write(socket->readLine());        
    }

    out.close();
    emit userMessage(name,"receiving " + file_name + " is complete");
    return true;
}


bool User::sendTextFile(const QString &command, const QString &file_name){
    QFile f(file_name);
    if ( !f.open(QIODevice::ReadOnly) ){
        emit userMessage(name," file " + file_name + " not found");
        return false;
    }
    while ( !f.atEnd() )
        send(command,f.readLine());

    f.close();
    return true;
}

void User::programResult(const QString &task, const QVector<qint32> *pPoints){
    QVector <qint32> points = *pPoints;
    sendPoints(task, points);
    if ( !points.isEmpty() )
        setPoints(server->getTaskList().indexOf(task),points[0]);
    delete pPoints;
}

void User::compilationResult(const QString &message){
    emit userMessage(name, message);
    send("message", message);
}

void User::send(const QString &command, const QString &message){
    socket->write(QString("/" + command + ":" + message + "\n").toUtf8());
}

void User::sendPoints(const QString &task, const QVector<qint32> &points){
    QString message;
    message += task + ":";
    foreach (qint32 point, points)
        message += QString::number(point) + ":";
    send("points",message);
}

void User::setName(const QString &_name){
    removeUserDir();
    name = _name;
    createUserDir();
    emit changeState();
}

void User::clearStatistic(){    
    for (int i = 0; i < statistic.count(); i++)
        statistic[i] = 0;
}

void User::setPoints(qint32 task_number, qint32 points){
    statistic_sum -= statistic[task_number];
    statistic_sum += points;
    statistic[task_number] = points;
    emit changeState();

}
void User::addPoints(qint32 task_number, qint32 points){
    statistic[task_number] += points;
    statistic_sum += points;
    emit changeState();
}

void User::subPoints(qint32 task_number, qint32 points){
    statistic[task_number] -= points;
    statistic_sum -= points;
    emit changeState();
}

QString User::getName(){
    return name;
}

QString User::getPointsSum(){
    return QString::number(statistic_sum);
}

QString User::getPointsTask(int task){
    return QString::number(statistic[task]);
}

bool User::isOnline(){
    return online;
}

void User::setOnline(bool status){
    online = status;
    emit changeState();
}

void User::sendExtension(){
    QMap <QString, bool> extensions;
    QDir  compilers_path("compilers");
    QFile compilers(compilers_path.filePath("compilers.json"));

    if ( !compilers.open(QIODevice::ReadOnly) ){
        emit userMessage(name, "compiler config not found");
        return;
    }
    QString json_data = compilers.readAll();
    QJsonDocument json_document = QJsonDocument::fromJson(json_data.toUtf8());
    compilers.close();
    QJsonObject json_root_object = json_document.object();
    QStringList json_compilers = json_root_object.keys();
    foreach (QString compiler, json_compilers) {
        QJsonObject json_compiler = json_root_object.value(compiler).toObject();
        QJsonArray  compiler_extensions = json_compiler.value("extensions").toArray();
        foreach (QVariant compiler_extension, compiler_extensions) {
            extensions[compiler_extension.toString()] = true;
        }
    }

    foreach(QString extension, extensions.keys()) {
        send("extension", extension);
        qDebug() << extension;
    }

    emit userMessage(name,"extensions is sent");
}
