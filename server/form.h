#ifndef FORM_H
#define FORM_H

#include <QWidget>

class Server;
class QTextEdit;
class QTableWidget;
class QPixmap;
class QPushButton;

class Form : public QWidget
{
    Q_OBJECT
    Server       *tcp_server;
    QTextEdit    *message_board;
    QPixmap      *user_icon;
    QTableWidget *users;

public:
    explicit Form(QWidget *parent = 0);
    void prepairTable();
protected:
    void closeEvent(QCloseEvent *event);
signals:

public slots:
    void userChangeState();
    void serverMessage(const QString &message);
    void userMessage  (const QString &name, const QString &message);
};

#endif // FORM_H
