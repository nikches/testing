QT += core widgets gui network

CONFIG -= console
TARGET = server

SOURCES += \
    main.cpp \
    form.cpp \
    server.cpp \
    user.cpp \
    thread.cpp \

HEADERS += \
    thread.h \
    form.h \
    server.h \
    user.h \
    platform.h

win32 {
    SOURCES += platform_win.cpp
}

linux{
    SOURCES += platform_linux.cpp
}
