#include "thread.h"
#include "user.h"
#include "platform.h"
#include <QtCore>
#include <QDebug>
#include <QJsonDocument>

Thread::Thread(const User *user, const QString &user_name, const QString &source) :QThread(0)
{
    m_source   = source;
    m_user     = user_name;
    connect(this,SIGNAL(programResult(QString,const QVector<qint32>*)),
            user,SLOT  (programResult(QString,const QVector<qint32>*)));

    connect(this,SIGNAL(compilationResult(QString)),user,SLOT(compilationResult(QString)));
    connect(this,SIGNAL(finished()),this,SLOT(deleteLater()));
}

Thread::~Thread(){
}

void Thread::run(){
    QString task = m_source.split(".").first();
    QVector <qint32> points;

    bool compile_result = buildProgram(m_user,m_source);
    if (!compile_result) return;

    points = runProgram(m_user,task);
    QVector <qint32> *pPoints = new QVector <qint32>(points);
    emit programResult(task,pPoints);
}

QString Thread::selectCompiler(const QString &user_dir, const QString &source){
    QString build_command;
    QString file_name      = source.split(".").first();
    QString file_extension = source.split(".").last();

    QDir  compiler_path("compilers");
    QFile compiler_config(compiler_path.filePath("compilers.json"));

    if ( !compiler_config.open(QIODevice::ReadOnly | QIODevice::Text) ){
        emit compilationResult("compiler config not found");
        return build_command;
    }

    QString json_data = compiler_config.readAll();
    compiler_config.close();

    QJsonDocument json_document = QJsonDocument::fromJson(json_data.toUtf8());
    QJsonObject json_root_object = json_document.object();
    QStringList compilers = json_root_object.keys();

    QJsonObject *selected_compiler = NULL;
    foreach (QString compiler, compilers) {
        if (selected_compiler != NULL) break;

        QJsonObject json_compiler = json_root_object.value(compiler).toObject();
        QJsonArray  compiler_extensions = json_compiler.value("extensions").toArray();

        foreach (QJsonValue compiler_extension, compiler_extensions) {
            if (compiler_extension.toString() == file_extension) {
                selected_compiler = &json_compiler;
                break;
            }
        }
    }

    if (selected_compiler == NULL ) {
        emit compilationResult("no supported extension");
        return build_command;
    }

    build_command = selected_compiler->value("path").toString();
    QDir dir("users");
    dir.cd(user_dir);
    foreach (QVariant option, selected_compiler->value("options").toArray()) {
        QString opt = option.toString();
        if (opt == "{executable}") {
            build_command += " " + dir.filePath(file_name);
        } else if (opt == "{source}") {
            build_command += " " + dir.filePath(source);
        } else {
            build_command += " " + option.toString() + " ";
        }
    }

    build_command = build_command.trimmed();
    return build_command;
}

bool Thread::buildProgram(const QString &user_dir, const QString &source){
    QString task = source.split(".").first();

    QProcess compile;
    QString build_command = selectCompiler(user_dir, source);
    emit compilationResult("starting: " + build_command);

    if (build_command.isEmpty()) {
        emit compilationResult("compilation: compiler settings not found");
        return false;
    }

    QDir executable_path("users");
    executable_path.cd(user_dir);
    if ( !executable_path.exists() ){
        emit compilationResult("compilation: user directory not found");
        return false;
    }

    QFile exe(executable_path.filePath(pl_name(task)));
    if (exe.exists()) exe.remove();

    compile.start(build_command);

    /* wait 30 sec */
    if (compile.waitForFinished() == false){
        emit compilationResult(QString("compilation: error"));
        return false;
    }

    if (exe.exists()){
        emit compilationResult("compilation: error");
        return true;
    }
    else {
        emit compilationResult("compilation: error");
        return false;
    }
}

bool Thread::compareFiles(const QString &file1, const QString &file2){
    QFile f1(file1);
    QFile f2(file2);

    if (f1.open(QIODevice::ReadOnly) == false) return false;
    if (f2.open(QIODevice::ReadOnly) == false) return false;

    QString line1;
    QString line2;
    while (f1.atEnd() == false && f2.atEnd() == false){
        line1 = f1.readLine().trimmed();
        line2 = f2.readLine().trimmed();

        if (line1 != line2) {
            f1.close();
            f2.close();
            return false;
        }
    }
    if (f1.atEnd() == false || f2.atEnd() == false){
        f1.close();
        f2.close();
        return false;
    }
    f1.close();
    f2.close();
    return true;
}

QVector <qint32> Thread::runProgram(const QString &user_dir, const QString &program){
    QVector <qint32> points;
    QDir             testing_path("tests");
    QDir             working_path = QDir::current();
    QProcess         process;


    testing_path.cd(program);
    working_path.cd("users");
    working_path.cd(user_dir);

    process.setWorkingDirectory(working_path.path());

    QFileInfoList list_in  = testing_path.entryInfoList(QStringList("in.*"), QDir::Files);
    QFileInfoList list_out = testing_path.entryInfoList(QStringList("out.*"), QDir::Files);

    points.push_back(0); // test completed
    points.push_back(0); // test count
    qint32 test_count    = 0;
    qint32 test_complete = 0;

    if (list_in.isEmpty() || list_in.isEmpty()){
        emit compilationResult("test dir is empty");
        return points;
    }

    QFile input_file_copy(working_path.filePath("input"));
    QFileInfoList::iterator it = list_out.begin();
    foreach (QFileInfo info, list_in){
        if (it == list_out.end())
            break;

        test_count++;

        if (input_file_copy.exists())
            input_file_copy.remove();

        QFile input_file(info.absoluteFilePath());
        input_file.copy (working_path.filePath("input"));


        process.start(working_path.filePath(program));

        if ( !process.waitForFinished(5000) ){
            points.push_back(1);
            it++;
            continue;
        }

        if (compareFiles(it->absoluteFilePath(),working_path.filePath("output"))){
            test_complete++;
            points.push_back(0);
        }
        else
            points.push_back(2);
        it++;
    }
    points[0] = test_complete;
    points[1] = test_count;
    emit compilationResult("run "  + program);
    return points;
}
