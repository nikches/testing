#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>

class User;
class QTcpSocket;

class Server : public QTcpServer
{
    Q_OBJECT
    qint32 user_count;

    QStringList                     task_list;
    QMap  <QTcpSocket *, User *>    users;
public:
    explicit Server(QObject *parent = 0);
    ~Server();

    qint32         getUserCount();
    QList <User *> getUsers();
    QStringList    getUserList();
    User *         getUser(QTcpSocket *client);
    QStringList    getTaskList();
    QString        checkName(const QString &name);

    void sendAll     (const QString &command, const QString &message);
    bool loadTaskList(const QString &name);


signals:
    void serverMessage(const QString &message);
public slots:
    void disconnected();
    void connected();
};

#endif // SERVER_H
