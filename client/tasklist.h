#ifndef TASKLIST_H
#define TASKLIST_H

#include <QListWidget>
#include <QMap>

class TaskList : public QListWidget
{
    Q_OBJECT
    QMap <QString, qint32> tasks;
public:
    explicit TaskList(QWidget *parent = 0);
    void addTask (const QString &task);
    void doneTask(const QString &task);
    void removeTask(const QString &task);
    void clearTask();
    void refreshTask();
signals:
    void onSend(const QString &);
public slots:

};

#endif // TASKLIST_H
