QT += gui network widgets core
CONFIG += console

SOURCES += \
    main.cpp \
    form.cpp \
    tasklist.cpp

HEADERS += \
    form.h \
    tasklist.h
