#include "form.h"
#include <tasklist.h>

#include <QtWidgets>
#include <QtNetwork>
#include <QMessageBox>
#include <QFileDialog>

Form::Form(QWidget *parent) :
    QWidget(parent)
{
    setWindowTitle("client");
    socket = new QTcpSocket(this);

    QVBoxLayout * main_layout    = new QVBoxLayout();
    QVBoxLayout * login_layout   = new QVBoxLayout();
    QGridLayout * testing_layout = new QGridLayout();

    login_frame      = new QFrame();
    testing_frame    = new QFrame();
    stacked_widget   = new QStackedWidget();
    status_bar       = new QStatusBar();

    login_frame   ->  setLayout(login_layout);
    testing_frame ->  setLayout(testing_layout);
    stacked_widget->  addWidget(login_frame);
    stacked_widget->  addWidget(testing_frame);
    main_layout   ->  addWidget(stacked_widget);
    main_layout   ->  addWidget(status_bar);

    setLayout(main_layout);

    connect_button = new QPushButton("connect");
    ip_addr        = new QLineEdit("localhost");
    port           = new QLineEdit("4200");
    user_name      = new QLineEdit();

    login_layout->addWidget(new QLabel("ip address"));
    login_layout->addWidget(ip_addr);
    login_layout->addWidget(new QLabel("port"));
    login_layout->addWidget(port);
    login_layout->addWidget(new QLabel("user name"));
    login_layout->addWidget(user_name);
    login_layout->addWidget(connect_button);



    send_message_button = new QPushButton(" send message");
    send_file_button    = new QPushButton(" send solution");
    task_info           = new QTextEdit();
    task_list           = new TaskList();
    task_result         = new QTextEdit();
    message_input       = new QTextEdit("/message:");

    task_info->setReadOnly(true);
    task_result->setReadOnly(true);

    QPixmap mail_pixmap("mail.png");
    QPixmap solution_pixmap("solution.png");
    send_message_button->setIcon(mail_pixmap);
    send_file_button->setIcon(solution_pixmap);

    testing_layout->addWidget(task_info,0,0,2,1);
    testing_layout->addWidget(task_list,0,1);
    testing_layout->addWidget(task_result,1,1);
    testing_layout->addWidget(message_input,2,0,2,1);
    testing_layout->addWidget(send_message_button,2,1);
    testing_layout->addWidget(send_file_button,3,1);

    connect(send_file_button,SIGNAL(clicked()),this,SLOT(onSendFile()));
    connect(task_list,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(itemClicked(QListWidgetItem*)));
    connect(connect_button,SIGNAL(clicked()),this,SLOT(onConnect()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(send_message_button,SIGNAL(clicked()),this,SLOT(onSend()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));

    restoreHostAddress("host.ini");
    setLoginFrame();
}



void Form::closeEvent(QCloseEvent *event){
     QMessageBox::StandardButton result;
     result = QMessageBox::question(this,"exit","you really want to quit?", QMessageBox::Yes | QMessageBox::No );
     if (result == QMessageBox::Yes){
         socket->disconnectFromHost();
         event->accept();
     }
     else
         event->ignore();
}


void Form::setLoginFrame(){
    task_list->clearTask();
    task_info->clear();
    stacked_widget->setCurrentWidget(login_frame);
    setGeometry(100,100,200,200);
}

void Form::setTestingFrame(){
    task_list->clearTask();
    task_info->clear();
    stacked_widget->setCurrentWidget(testing_frame);
    setGeometry(100,100,700,500);
}

void Form::onConnect(){
    if (user_name->text().isEmpty()){
        status_bar->showMessage("please, enter your name");
        user_name->setFocus();
        return;
    }
    if (ip_addr->text().isEmpty()){
        status_bar->showMessage("please, enter host name");
        ip_addr->setFocus();
        return;
    }
    if (port->text().isEmpty()){
        status_bar->showMessage("please, enter port");
        port->setFocus();
        return;
    }

    QString user_name_message = "/connect:" + user_name->text() + "\n";
    socket->connectToHost(ip_addr->text(),port->text().toInt());
    if (socket->waitForConnected()){
        status_bar->showMessage("connection to server");
        // send name
        socket->write(user_name_message.toUtf8());

        // send request to given task list
        socket->write(QString("/tasklist:\n").toUtf8());

        // send request to given extension list
        socket->write(QString("/extension:\n").toUtf8());

        storeHostAddress("host.ini");
        setWindowTitle("client " + user_name->text());
        setTestingFrame();
        status_bar->clearMessage();
    }
    else
        status_bar->showMessage("server not found");
}

void Form::clearData(){
    extension_set.clear();
    task_list->clear();
    task_result->clear();
    task_info->clear();
}

void Form::onSend(){
    if (message_input->toPlainText() == "/message:") return;
    QString message = message_input->toPlainText() + "\n";
    socket->write(message.toUtf8());
    message_input->setText("/message:");
}

void Form::disconnected(){
    status_bar->showMessage("disconneced from server");
    clearData();
    setLoginFrame();
}

void Form::readyRead(){
    QTcpSocket *server = (QTcpSocket*)sender();
    QString line;
    QString command;
    QString text;

    QRegExp regExp("^/([a-z]{1,16}):(.*)$");

    while (server->canReadLine()){
        line = QString::fromUtf8(server->readLine()).trimmed();
        if (regExp.indexIn(line) != -1){
            command = regExp.cap(1);
            text    = regExp.cap(2);

            if (command == "message"){
                task_result->append("MESSAGE:");
                task_result->append(text);
                task_result->append("");
                continue;
            }

            if (command == "tasklist"){
                task_list->addTask(text);
                continue;
            }

            if (command == "task"){
                task_info->append(text);
                continue;
            }

            if (command == "error"){
                task_result->append("ERROR:");
                task_result->append(text);
                task_result->append("");
                continue;
            }
            if (command == "points"){
                processingResult(text);
                continue;
            }

            if (command == "extension"){
                extension_set << text;
                continue;
            }
        }
    }
}

void Form::storeHostAddress(const QString &path){
    QFile ini_file(path);
    QTextStream out(&ini_file);
    if (ini_file.open(QIODevice::WriteOnly)){
        out << "[ip address]\n";
        out << ip_addr->text() << "\n";
        out << "[port]\n";
        out << port->text() << "\n";
        ini_file.close();
    }
}

void Form::restoreHostAddress(const QString &path){
    QFile ini_file(path);
    QTextStream in(&ini_file);
    QString line;
    if ( !ini_file.open(QIODevice::ReadOnly) )
        return;
    while ( !ini_file.atEnd() ){
       line = in.readLine().trimmed();
       if (line == "[ip address]")
          ip_addr->setText(in.readLine());
       if (line == "[port]")
          port->setText(in.readLine());
    }
    ini_file.close();
}

void Form::itemClicked(QListWidgetItem *item){
    task_info->clear();
    QString message = "/task:" + item->text() + "\n";
    socket->write(message.toUtf8());
}

void Form::onSendFile(){
    QString filter = "sources (";
    foreach (QString extension, extension_set)
        filter += "*." + extension + " ";
    filter += ")";

    if (task_list->currentItem())
    sendFile(QFileDialog::getOpenFileName(this,"send file","",filter),
             task_list->currentItem()->text());    
    else{
        task_result->append("INFO");
        task_result->append("task not selected");
        task_result->append("");
    }
}

void Form::processingResult(const QString &result){
    QStringList list = result.split(":");

    QStringList state;
    state << "accepted" << "runtime error" << "wrong answer";
    task_result->append("SOLUTION RESULT:");
    task_result->append(list[0] + ": (" + list[1]+ "/"+list[2] + ")");
    for (int i = 3; i < list.count()-1; i++)
        task_result->append("test " + QString::number(i-3) + ": " + state[list[i].toInt()]);
    task_result->append("");
    if (list[1] == list[2])
        task_list->doneTask(list[0]);
}

void Form::sendFile(const QString  &path,const QString &task_name){
    QStringList name = path.split(".");
    if (name.count() == 1){
        task_result->append("INFO");
        task_result->append("File no have extension");
        task_result->append("");
        return;
    }
    if (!extension_set.contains(name.last())){
        task_result->append("INFO");
        task_result->append("Incorrect extension (" + name.last() + ")");
        task_result->append("");
        return;
    }

    QFile in(path);    
    if ( !in.open(QIODevice::ReadOnly) )
        return;
    QStringList lines;
    while ( !in.atEnd() )
       lines << in.readLine();

    if ( !lines.isEmpty() )
        lines.last().append("\n");

    socket->write(QString("/file:"+ task_name +
                          "." + name.last()   +
                          ":" + QString::number(lines.count()) +
                          "\n").toUtf8());
    foreach (QString line, lines)
        socket->write(line.toUtf8());

    in.close();   
    task_result->append("INFO");
    task_result->append(task_name + "." + name.last() +  " is sent");
    task_result->append("");
}
