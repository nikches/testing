#include "tasklist.h"

TaskList::TaskList(QWidget *parent) :
    QListWidget(parent)
{
    setIconSize(QSize(16,16));
}


void TaskList::addTask(const QString &task_name){
    tasks[task_name] = 0;
    refreshTask();
}

void TaskList::doneTask(const QString &task_name){
    tasks[task_name] = 1;
    refreshTask();
}

void TaskList::removeTask(const QString &task){
    tasks.remove(task);
    refreshTask();
}

void TaskList::clearTask(){
    tasks.clear();
    refreshTask();
}

void TaskList::refreshTask(){
    clear();
    QListWidgetItem *item;
    QPixmap pixmap_ok("ok.png");
    QPixmap pixmap_ready("ready.png");
    QMapIterator <QString,qint32> it(tasks);

    while (it.hasNext()){
        it.next();
        item = new QListWidgetItem(it.key(),this);
        if (it.value() == 0) item->setIcon(pixmap_ready);
        else item->setIcon(pixmap_ok);
    }

}
