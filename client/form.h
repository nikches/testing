#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QMap>
#include <QSet>

class QListWidgetItem;
class QPushButton;
class QTcpSocket;
class QLineEdit;
class QStatusBar;
class QStackedWidget;
class QTextEdit;
class TaskList;
class QFrame;

class Form : public QWidget
{
    Q_OBJECT
    QTcpSocket     *socket;
    QSet <QString> extension_set;
    // layout
    QStatusBar     *status_bar;
    QStackedWidget *stacked_widget;
    QFrame         *login_frame;
    QFrame         *testing_frame;

    // login frame
    QPushButton    *connect_button;
    QLineEdit      *ip_addr;
    QLineEdit      *port;
    QLineEdit      *user_name;
    // testing frame

    QTextEdit      *message_input;
    TaskList       *task_list;
    QTextEdit      *task_info;
    QTextEdit      *task_result;
    QPushButton    *send_message_button;
    QPushButton    *send_file_button;

public:
    explicit Form(QWidget *parent = 0);
    void storeHostAddress(const QString &path);
    void restoreHostAddress(const QString &path);
    void setLoginFrame();
    void setTestingFrame();
    void sendFile(const QString &path,const QString &name);
    void clearData();

protected:
    void processingResult(const QString &result);
    void closeEvent(QCloseEvent *event);

signals:

public slots:
    void onConnect();
    void onSend();
    void onSendFile();

    void disconnected();
    void readyRead();
    void itemClicked(QListWidgetItem *);
};

#endif // FORM_H
